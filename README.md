# Tera
A WIP video codec that is being developed in collaboration with [metamuffin](https://codeberg.org/metamuffin/video-codec-experiments/src/branch/master/evc) and written in C++. This codec aims to solve all issues of my previous video codec, [versio](https://gitlab.com/tpart/versio). The codec is NOT YET READY for usage and is highly experimental.

## License
This project is licensed under the GPL-3.0-or-later license.


Tera
Copyright (C) 2022 tpart

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.