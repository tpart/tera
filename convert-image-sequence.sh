#!/bin/bash
ffmpeg -framerate 25 -pattern_type glob -i '*.ppm' \
  -c:v libvpx-vp9 -b:v 10M -pix_fmt yuv420p output-noaudio.webm
ffmpeg -i output-noaudio.webm -i audio.ogg -c copy -map 0:v:0 -map 1:a:0 output.webm
