#pragma once
#include "Area.h"
#include "Pixel.h"
#include <sys/types.h>
#include <vector>

class Image {
  public:
	std::vector<Pixel> pixels;

	Image(std::vector<Pixel> pixels);
	Image();
	Pixel& getPixel(uint x, uint y, uint16_t& imageWidth);
	void setPixel(Pixel& newPixel, uint x, uint y, uint16_t& imageWidth);
	void printPixels(int &pixelCount);
	void saveAsFile(const char *outputFile, uint16_t& imageHeight, uint16_t& imageWidth);
	void drawDebugArea(Area& area, uint16_t& imageHeight, uint16_t& imageWidth);
};