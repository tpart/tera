#pragma once

#include "Vector2.h"
#include <cstddef>
#include <cstdint>
#include <sys/types.h>
#include<tuple>

class Area {
  public:
    int startX;
    int endX;
    int startY;
    int endY;
    bool replacePixels = false;
    bool hasChild = false;
    std::tuple<Area*, Area*> childAreas;
    Vector2 bestTranslation;

    Area(int startX, int startY, int endX, int endY);
    std::tuple<Area*, Area*> splitArea();
    uint getSizeX();
    uint getSizeY();
    void printArea();
};