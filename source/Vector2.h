#pragma once

#include <cstdint>
#include <sys/types.h>

class Vector2 {
  public:
    int x;
    int y;

    Vector2(int x, int y);
    Vector2();
};