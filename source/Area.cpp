#include "Area.h"
#include "Vector2.h"
#include <iostream>
#include <tuple>

Area::Area(int startX, int startY, int endX, int endY) {
	this->startX = startX;
	this->startY = startY;
	this->endX = endX;
	this->endY = endY;
	this->bestTranslation = Vector2();
}

std::tuple<Area*, Area*> Area::splitArea() {
	if (endX - startX > endY - startY) {
		// Split block vertically
		uint splitSizeX = getSizeX() / 2;
		Area* area1 = new Area(startX, startY, startX + splitSizeX, endY);
		Area* area2 = new Area(startX + splitSizeX, startY, endX, endY);
		return std::make_tuple(area1, area2);
	}
	else {
		// Split block horizontally
		uint splitSizeY = getSizeY() / 2;
		Area* area1 = new Area(startX, startY, endX, startY + splitSizeY);
		Area* area2 = new Area(startX, startY + splitSizeY, endX, endY);
		return std::make_tuple(area1, area2);
	}
}

uint Area::getSizeX() {
	return endX - startX;
}

uint Area::getSizeY() {
	return endY - startY;
}

void Area::printArea() {
	std::cout << "Area: (" << startX << ", " << startY << ") to (" << endX << ", " << endY << ")\n";
}