#include "Pixel.h"
#include <cstdint>
#include <iostream>
#include <tuple>

Pixel::Pixel(unsigned char r, unsigned char g, unsigned char b) {
	this->r = r;
	this->g = g;
	this->b = b;
}

Pixel::Pixel() {
	this->r = 0;
	this->g = 0;
	this->b = 0;
}

std::tuple<uint8_t, uint8_t, uint8_t> Pixel::toBytes() {
	return std::make_tuple(r, g, b);
}

char Pixel::getAverage() {
	return (char)((uint16_t)r + (uint16_t)g + (uint16_t)b / (uint16_t)3);
}

int Pixel::getDiff(Pixel& otherPixel) {
	int returnValue = fastAbsDiff(r, otherPixel.r) + fastAbsDiff(g, otherPixel.g)
	                      + fastAbsDiff(b, otherPixel.b);
	return returnValue;
}

int Pixel::fastAbsDiff(uint8_t val, uint8_t val2) {
	if (val >= val2) {
		return (int)(val - val2);
	}
	else {
		return (int)(val2 - val);
	}
}